
# Login mysql via user Root
$ mysql -u  root

# Create user, database support run project
$ CREATE DATABASE menagerie;
$ CREATE USER 'redmine'@'localhost' IDENTIFIED BY 'my_password';
$ GRANT ALL PRIVILEGES ON * . * TO 'redmine'@'localhost';

# Setup dependencies
$ bundle exec bundle install
$ bundle exec rake db:migrate:rest RAILS_ENV=development
$ bundle exec rake redmine:plugins:migrate RAILS_ENV=development

# Run project on port 3000
$ bundle exec rails s

# Run project on other port
$ bundle exec rails s -p #{port}

# Access project and register new user
http://localhost:3000/

# Set admin privileges and active for newly created users
$ bundle exec rails c
 > user = User.last 
 > user.admin = true
 > user.status = 1
 > user.save
 > exit 

# Re-login and access via admin user ( http://localhost:3000/ )


